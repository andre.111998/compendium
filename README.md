# Compendium 

Compendium is a library allowing maps containing various types to be properly stored, removed, or searched for. It will also allow the maps to be serialized and deserialized.