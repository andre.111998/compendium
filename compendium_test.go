package compendium

import (
	"fmt"
	"testing"
)

func NewDicitionary() Compendium[string] {
	return Compendium[string]{}
}

func NewMenu() Compendium[mockFood] {
	return Compendium[mockFood]{}
}

func TestCompendium(t *testing.T) {
	mockDictionary := NewDicitionary()

	addMap := map[string]string{
		"apple":  "a round edible fruit",
		"carrot": "a narrow orange-colored root eaten as a vegetable",
	}

	// add initial 2 words

	for k, v := range addMap {
		ok := mockDictionary.Add(k, v)
		if !ok {
			t.Fatalf(`adding initial 2 words: expected the bool value to be true when adding the word %v`, k)
		}
	}

	length := len(mockDictionary)
	if length != 2 {
		t.Fatalf(`adding initial 2 words: expected compendium length to be 2, got %v instead`, length)
	}

	// add third word

	ok := mockDictionary.Add("steak", "high quality beef taken from the hindquaters of the animal")
	if !ok {
		t.Fatalf(`adding third word: expected the bool value to be true when adding the word steak`)
	}

	length = len(mockDictionary)
	if length != 3 {
		t.Fatalf(`adding ithird word: expected compendium length to be 3, got %v instead`, length)
	}

	// add exisiting word

	ok = mockDictionary.Add("apple", "a round edible fruit grown from apple tree")
	if ok {
		t.Fatalf(`adding existing word: expected the bool value to be false when adding the exisiting word apple`)
	}

	length = len(mockDictionary)
	if length != 3 {
		t.Fatalf(`adding existing word: expected compendium length to be 3, got %v instead`, length)
	}

	// add exisiting word override

	mockDictionary.AddOverride("apple", "a round edible fruit grown from apple tree")

	length = len(mockDictionary)
	if length != 3 {
		t.Fatalf(`adding existing word override: expected compendium length to be 3, got %v instead`, length)
	}

	// remove exisiting word

	ok = mockDictionary.Remove("carrot")
	if !ok {
		t.Fatalf(`removing existing word: expected the bool value to be true when removing the exisiting word carrot`)
	}

	length = len(mockDictionary)
	if length != 2 {
		t.Fatalf(`removing existing word: expected compendium length to be 2, got %v instead`, length)
	}

	// remove non-exisiting word

	ok = mockDictionary.Remove("porkchop")
	if ok {
		t.Fatalf(`removing non-existing word: expected the bool value to be false when removing the non-exisiting word porkchop`)
	}

	length = len(mockDictionary)
	if length != 2 {
		t.Fatalf(`removing non-existing word: expected compendium length to be 2, got %v instead`, length)
	}

	// search for entries
	mockDictionary.Add("app", "an application")
	list := mockDictionary.Search("app")

	length = len(list)
	if length != 2 {
		t.Fatalf(`searching for entries: expected search list length to be 2, got %v instead`, length)
	}
	if list[0] != "app" {
		t.Fatalf(`searching for entries: expected entry 1 of list to be app, got %v instead`, list[0])
	}
	if list[1] != "apple" {
		t.Fatalf(`searching for entries: expected entry 2 of list to be apple, got %v instead`, list[1])
	}

	// search slightly miss-spelled for entries
	list = mockDictionary.Search("aple")

	length = len(list)
	if length != 1 {
		t.Fatalf(`searching for entries: expected search list length to be 1, got %v instead`, length)
	}
	if list[0] != "apple" {
		t.Fatalf(`searching for entries: expected entry 1 of list to be apple, got %v instead`, list[0])
	}

}

type mockFood struct {
	Meal  string `json:"meal"`
	Price int    `json:"price"`
}

func TestCompendiumUnmarshaller(t *testing.T) {
	bytes := []byte(`{"eggs": {"meal": "breakfast", "price": 7}, "burger": {"meal": "lunch", "price": 16}, "steak": {"meal": "dinner", "price": 28}}`)
	mockMenu := NewMenu()
	err := mockMenu.UnmarshalJSON(bytes)
	if err != nil {
		t.Fatalf(fmt.Sprintf("%s", err))
	}

	badBytes := []byte(`{"meal": "breakfast", "price": 7}`)
	mockMenu = NewMenu()
	err = mockMenu.UnmarshalJSON(badBytes)
	if err == nil {
		t.Fatalf("Expected bad unmarshal")
	}

}
