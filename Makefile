PROJECT_NAME := "compendium"
PKG := "gitlab.com/andre.111998/$(PROJECT_NAME)"
PKG_LIST := $(shell go list ${PKG}/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
PKG_VERSION=$(shell echo v$$(jq .version package.json))

hello:
	@echo "Building Compendium"

build: dep
	@go build -v

dep:
	@go get -v -d ./...

run:
	@go run main.go

clean:
	@rm -f $(PROJECT_NAME)

test:
	@go test -cover ${PKG_LIST}

lint:
	@golangci-lint run --enable-all ${PKG_LIST}

lint-blocking:
	@golangci-lint run ${PKG_LIST}

tidy:
	@go mod tidy

all: hello lint tidy test build



