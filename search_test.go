package compendium

import "testing"

func TestCompareStrings(t *testing.T) {
	cases := map[string]struct {
		str1       string
		str2       string
		similarity float32
	}{
		"same words": {
			str1:       "apple",
			str2:       "apple",
			similarity: 1,
		},
		"slighty miss-spelled words": {
			str1:       "apple",
			str2:       "aple",
			similarity: 6.0 / 7.0,
		},
		"ignore spaces": {
			str1:       "apple juice",
			str2:       "applejuice",
			similarity: 1,
		},
		"2nd word contains first word": {
			str1:       "apple",
			str2:       "apple juice",
			similarity: 8.0 / 13.0,
		},
		"repeated bigrams": {
			str1:       "banana",
			str2:       "ban",
			similarity: 4.0 / 7.0,
		},
	}

	for name, test := range cases {
		similarity := CompareStrings(test.str1, test.str2)
		if similarity != test.similarity {
			t.Fatalf(`%s: expected similarity of %.2f%%, got %.2f%% instead`, name, test.similarity*100, similarity*100)
		}
	}
}
