package compendium

import (
	"fmt"
	"strings"
)

func CompareStrings(str1, str2 string) float32 {
	removeSpaces(&str1, &str2)

	bigramChars := make(map[string]int)
	var intersectionSize float32
	for i := 0; i < len(str1)-1; i++ {
		a := fmt.Sprintf("%c", str1[i])
		b := fmt.Sprintf("%c", str1[i+1])

		bigram := a + b

		bigramChars[bigram]++
	}

	for i := 0; i < len(str2)-1; i++ {
		a := fmt.Sprintf("%c", str2[i])
		b := fmt.Sprintf("%c", str2[i+1])

		bigram := a + b

		if bigramChars[bigram] > 0 {
			bigramChars[bigram]--
			intersectionSize++
		}
	}

	return (2.0 * intersectionSize) / (float32(len(str1)) + float32(len(str2)) - 2)
}

func removeSpaces(str1, str2 *string) {
	*str1 = strings.Replace(*str1, " ", "", -1)
	*str2 = strings.Replace(*str2, " ", "", -1)
}
