package compendium

import (
	"encoding/json"
	"sort"
	"strings"
)

const (
	stringSimilarityThreshold = 0.5
)

// Compendium is a generic collection of types that allows users
// to add, remove, and search for.
type Compendium[T any] map[string]T

type CompendiumMap interface {
	Add(key string, entry any) bool
	AddOveride(key string, entry any)
	Contains(key string) bool
	Remove(key string) bool
	Search(key string) []any
	UnmarshalJSON(bytes []byte) error
}

// Add adds a new entry type to the specfied key index.
// If an entry already exists for the provide key index,
// entry is not added and returns false.
func (c Compendium[T]) Add(key string, entry T) bool {
	if ok := c.Contains(key); ok {
		return false
	}
	c[key] = entry
	return true
}

// AddOverride adds a new entry type to the specfied key index
// reguardless if an entry already exists for the proivded key index.
func (c Compendium[T]) AddOverride(key string, entry T) {
	c[key] = entry
}

// Contains checks if the provided key index exists in the Compendium.
func (c Compendium[T]) Contains(key string) bool {
	_, ok := c[key]
	return ok
}

// Remove removes the specfied key entry of the specifed
// key value. Returns false if entry doesn't exist.
func (c Compendium[T]) Remove(key string) bool {
	if _, ok := c[key]; ok {
		delete(c, key)
		return true
	}
	return false
}

// Search searches for closely releated key indeces and returns
// a list of entries.
func (c Compendium[T]) Search(key string) []string {
	list := []string{}
	for k := range c {
		if strings.Contains(k, key) {
			list = append(list, k)
		} else if similarity := CompareStrings(k, key); similarity >= stringSimilarityThreshold {
			list = append(list, k)
		}
	}
	sort.Strings(list)
	return list
}

// UnmarshalJSON implements the json.Unmarshaler interface.
func (c *Compendium[T]) UnmarshalJSON(bytes []byte) error {
	var m map[string]T

	if err := json.Unmarshal(bytes, &m); err != nil {
		return err
	}

	*c = Compendium[T](m)

	return nil
}
