# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.1.1] - August 2023
### Added
- Implemented string comparitor
  - A string is considered closely matched to another string when at least 50% related in bigrams
    (the current set threshold).

[0.1.1]: https://gitlab.com/andre.111998/compendium/-/merge_requests/2

## [0.1.0] - August 2023
### Added
- Initial Compendium implemenation

[0.1.0]: https://gitlab.com/andre.111998/compendium/-/merge_requests/1